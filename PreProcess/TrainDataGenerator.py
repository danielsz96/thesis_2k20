import fnmatch
import csv
import scipy.ndimage
from tqdm import tqdm
from pylidc.utils import consensus
from Utility.PreProcessUtils import *


class TrainDataGenerator(object):
    def __init__(self, dataset_directory, output_path, should_return_non_nodule_slices):
        self.dataset_directory = dataset_directory
        self.output_path = output_path
        self.should_return_non_nodule_slices = should_return_non_nodule_slices

    zarr_dir_path = "/home/szabda/thesis_2020/training_bin/zarrTrainingDataset.zarr"
    zarr_training_group_name = 'training'
    zarr_validation_group_name = 'validation'
    zarr_scan_array_name = 'scans'
    zarr_mask_array_name = 'masks'


    def generate_compressed(self, output_file_name, should_generate_images=False):
        """Run the training data generator.

        ARGUMENTS
        -----------
        output_file_name: string
            Name of the
        should_generate_images: bool
            Should the function generate npy image and mask arrays, or mask images

        RETURNS
        -----------
        None

        WRITES
        -----------
        None
        """

        if not should_generate_images:
            root, zarr_scan_array, zarr_mask_array = create_or_open_zarr_arrays()

            csv_data_path = '/home/szabda/thesis_2020/training_bin/zarr_data_info.csv'
            saved_scan_series_instance_uid = np.array([])

            if not os.path.exists(csv_data_path):
                with open(csv_data_path, 'a', newline='') as file:
                    writer = csv.writer(file)
                    writer.writerow(['series_instance_uid'])

            else:
                with open(csv_data_path, 'r', newline='') as file:
                    table = csv.reader(file, delimiter=',')
                    for index, row in enumerate(table):
                        # skip the header row
                        if index == 0:
                            continue

                        saved_scan_series_instance_uid = np.append(saved_scan_series_instance_uid, row[0])

        scan_counter = 0
        for subdir, dirs, files in os.walk(self.dataset_directory):
            #print('current subdirectory path:', subdir)
            mhd_counter = len(fnmatch.filter(os.listdir(subdir), '*.mhd'))
            with tqdm(total=mhd_counter) as progress_bar:
                for file in files:
                    series_instance_uid = os.path.splitext(file)[0].lower()
                    if saved_scan_series_instance_uid.size != 0 and\
                            np.isin(series_instance_uid, saved_scan_series_instance_uid):
                        scan_counter += 1
                        progress_bar.update(1)
                        continue

                    filepath = subdir + os.sep + file

                    if filepath.endswith(".mhd"):
                        if should_generate_images:
                            mask_image = self.get_nodule_mask_for(series_instance_uid=series_instance_uid,
                                                                  dataset_directory=subdir,
                                                                  should_return_image=True)

                            del mask_image
                            gc.collect()

                        else:
                            mask_array_item, img_array_item = \
                                self.get_nodule_mask_for(series_instance_uid=series_instance_uid,
                                                         dataset_directory=subdir,
                                                         should_return_image=False)

                            segmented_image_array = segment_lung_from_scan(img_array_item)

                            #save a demo slice to show the segmentation process and accuray
                            if scan_counter % 15 == 0:
                                save_lung_segmentation_demo_image(img_array_item, series_instance_uid)

                            zarr_scan_array.append(segmented_image_array[np.newaxis,:])
                            zarr_mask_array.append(mask_array_item[np.newaxis,:])

                            with open('/home/szabda/thesis_2020/training_bin/zarr_data_info.csv', 'a', newline='') as file:
                                writer = csv.writer(file)
                                writer.writerow([series_instance_uid])

                            del img_array_item,
                            mask_array_item,
                            segmented_image_array
                            gc.collect()

                        scan_counter += 1

                        del series_instance_uid

                        progress_bar.update(1)

                    del filepath
                    gc.collect()

        print('\\=========\\')
        print(root.tree(expand=True))
        print('root_info:\n\n', root.info)
        print(zarr_scan_array.info)
        print(zarr_mask_array.info)
        print('\\=========\\')


    def get_nodule_mask_for(self, series_instance_uid,
                            dataset_directory,
                            should_return_image=False,
                            resize_factor=1.0):
        # TODO: create a method to resample the image at the beginning
        #       issue: now resampling corrupts the annotations
        """Get mask associated with the current CT scan (series_instance_uid).

        ARGUMENTS
        -----------
        series_instance_uid: string
            Scan's series_instance_uid
        dataset_directory: string
            Path of the dataset
        should_generate_images: bool
            Should the function generate ndarray image and mask array, or mask image
        resize_factor: float
            The resize factor that the generated arrays or image should be resized with
            along the x & y axes

        RETURNS
        -----------
        should_return_image ? (mask_image: image) : (mask_array: ndarray, image_array: ndarray)
            Mask associated with the current CT scan

        WRITES
        ----------
        None
        """
        # get current image as numpy.ndarray
        itk_img, img_array = \
            read_image_with_sitk_for_path(os.path.join(dataset_directory, series_instance_uid) + '.mhd')
        # save dimensions of image
        num_z, height, width = img_array.shape

        # get nodules for current scan
        nods, pixel_spacing, slice_thickness, slice_spacing = get_required_metadata_for(series_instance_uid)

        # Allocate space for the masks
        luna_mask = np.full([num_z, height, width], fill_value=0, dtype=np.uint8)

        for anns in nods:
            # Perform a consensus consolidation and 50% agreement level.
            # We pad the slices to add context for viewing.
            cmask, cbbox, _ = consensus(anns)

            # Bounding box boundary coordinates
            z_min = cbbox[2].start
            z_max = cbbox[2].stop
            x_min = cbbox[1].start
            x_max = cbbox[1].stop
            y_min = cbbox[0].start
            y_max = cbbox[0].stop

            # The consensus function returns the consensus mask as a bool array
            # The following row changes it to numeric values
            # Change coordinates to z, y, x order
            cmask = np.transpose(cmask)
            cmask = np.swapaxes(cmask, 1, 2)

            # Updating luna_mask array with the mask of the current nodule
            # If there are multiple nodules, the luna_mask array
            # gets updated
            luna_mask_current = luna_mask[z_min:z_max, y_min:y_max, x_min:x_max].copy()
            luna_mask[z_min:z_max, y_min:y_max, x_min:x_max] = np.maximum(cmask, luna_mask_current)

            del cmask,
            cbbox,
            z_min,
            z_max,
            x_min,
            y_min,
            y_max,
            luna_mask_current
            gc.collect()

        if should_return_image:
            # Creating SITK image object to save the mask .mhd file
            if resize_factor != 1.0:
                try:
                    luna_mask = scipy.ndimage.zoom(luna_mask, (1, resize_factor, resize_factor), mode='nearest')
                except:
                    print(f'Failed to resample luna_mask with factor: {resize_factor} for Uid: {series_instance_uid}')

            try:
                new_image = sitk.GetImageFromArray(luna_mask)
                new_image.CopyInformation(itk_img)
            except:
                print(f'Failed to create image from luna_mask using\
                      sitk.GetImageFromArray for Uid: {series_instance_uid}')

            try:
                return new_image
            finally:
                del new_image,
                num_z,
                height,
                width,
                img_array,
                nods,
                luna_mask
                gc.collect()

        else:
            if resize_factor != 1.0:
                try:
                    luna_mask = scipy.ndimage.zoom(luna_mask, (1, resize_factor, resize_factor), mode='nearest')
                except:
                    print(f'Failed to resample luna_mask with factor: {resize_factor} for Uid: {series_instance_uid}')

                try:
                    img_array = scipy.ndimage.zoom(img_array, (1, resize_factor, resize_factor), mode='nearest')
                except:
                    print(f'Failed to resample img_array with factor: {resize_factor} for Uid: {series_instance_uid}')

            if not self.should_return_non_nodule_slices:
                # It would be computationally more efficient if we filtered the arrays before resizing,
                # but in that case we would get "false positive" masks, because the size of the nodule
                # can be so small, the nearest neighboring method simply cuts out the positive part of the mask
                img_array, luna_mask = self.filter_nodule_slices(img_array, luna_mask)

            img_array = resample(img_array, slice_thickness, pixel_spacing, fill_value=-2000)
            luna_mask = resample(luna_mask, slice_thickness, pixel_spacing, fill_value=0)

            try:
                return luna_mask, img_array
            finally:
                del num_z,
                height,
                width,
                img_array,
                nods,
                luna_mask
                gc.collect()
