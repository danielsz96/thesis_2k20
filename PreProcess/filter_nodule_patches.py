import zarr
import os
import numpy as np
import json
from joblib import Parallel, delayed


def fix_out_of_bound_patch_attempt(data, patch_shape, patch_index, ndim=3):

    image_shape = data.shape[-ndim:]
    pad_before = np.abs((patch_index < 0) * patch_index)
    pad_after = np.abs(((patch_index + patch_shape) > image_shape) * ((patch_index + patch_shape) - image_shape))
    pad_args = np.stack([pad_before, pad_after], axis=1)
    if pad_args.shape[0] < len(data.shape):
        pad_args = [[0, 0]] * (len(data.shape) - pad_args.shape[0]) + pad_args.tolist()
    data = np.pad(data, pad_args, mode="edge")
    patch_index += pad_before
    return data, patch_index


def compute_patch_indices(image_shape, patch_size, overlap, start=None):
    if isinstance(overlap, int):
        overlap = np.asarray([overlap] * len(image_shape))
    if start is None:
        n_patches = np.ceil(image_shape / (patch_size - overlap))
        overflow = (patch_size - overlap) * n_patches - image_shape + overlap
        start = -np.ceil(overflow/2)
    elif isinstance(start, int):
        start = np.asarray([start] * len(image_shape))
    stop = image_shape + start
    step = patch_size - overlap
    return get_set_of_patch_indices(start, stop, step)


def get_set_of_patch_indices(start, stop, step):
    return np.asarray(np.mgrid[start[0]:stop[0]:step[0], start[1]:stop[1]:step[1],
                               start[2]:stop[2]:step[2]].reshape(3, -1).T, dtype=np.int)




def get_patch_from_3d_data(data, patch_shape, patch_index):

    patch_index = np.asarray(patch_index, dtype=np.int16)
    patch_shape = np.asarray(patch_shape)
    image_shape = data.shape[-3:]
    if np.any(patch_index < 0) or np.any((patch_index + patch_shape) > image_shape):
        data, patch_index = fix_out_of_bound_patch_attempt(data, patch_shape, patch_index)
    return data[..., patch_index[0]:patch_index[0]+patch_shape[0], patch_index[1]:patch_index[1]+patch_shape[1],
                patch_index[2]:patch_index[2]+patch_shape[2]]





def reconstruct_from_patches(patches, patch_indices, data_shape, default_value=0):
   
    data = np.ones(data_shape) * default_value
    image_shape = data_shape[-3:]
    count = np.zeros(data_shape, dtype=np.int)
    for patch, index in zip(patches, patch_indices):
        image_patch_shape = patch.shape[-3:]
        if np.any(index < 0):
            fix_patch = np.asarray((index < 0) * np.abs(index), dtype=np.int)
            patch = patch[..., fix_patch[0]:, fix_patch[1]:, fix_patch[2]:]
            index[index < 0] = 0
        if np.any((index + image_patch_shape) >= image_shape):
            fix_patch = np.asarray(image_patch_shape - (((index + image_patch_shape) >= image_shape)
                                                        * ((index + image_patch_shape) - image_shape)), dtype=np.int)
            patch = patch[..., :fix_patch[0], :fix_patch[1], :fix_patch[2]]
        patch_index = np.zeros(data_shape, dtype=np.bool)
        patch_index[...,
                    index[0]:index[0]+patch.shape[-3],
                    index[1]:index[1]+patch.shape[-2],
                    index[2]:index[2]+patch.shape[-1]] = True
        patch_data = np.zeros(data_shape)
        patch_data[patch_index] = patch.flatten()

        new_data_index = np.logical_and(patch_index, np.logical_not(count > 0))
        data[new_data_index] = patch_data[new_data_index]

        averaged_data_index = np.logical_and(patch_index, count > 0)
        if np.any(averaged_data_index):
            data[averaged_data_index] = (data[averaged_data_index] * count[averaged_data_index] + patch_data[averaged_data_index]) / (count[averaged_data_index] + 1)
        count[patch_index] += 1
    return data


def get_patch_from_scan_and_mask_array(scan_array, mask_array, scan_number, patch_shape, patch_index_list, patch_index):
    scan_patch = get_patch_from_3d_data(scan_array[scan_number], patch_shape, patch_index_list[patch_index])
    mask_patch = get_patch_from_3d_data(mask_array[scan_number], patch_shape, patch_index_list[patch_index])
    print(patch_index_list[patch_index])
    return scan_patch, mask_patch


def get_patch_from_mask_array(mask_array, patch_shape, patch_index):
    mask_patch = get_patch_from_3d_data(mask_array, patch_shape, patch_index)
    # print(patch_index)
    return mask_patch




# for scan_num, mask in enumerate(zarr_mask_array):
# 
#     print("filtering scan number " + str(scan_num))
#     for patch_index in indexek:
#         actual_mask_patch = get_patch_from_mask_array(mask, (64,64,64), patch_index)
#         patch_index = np.append(scan_num, patch_index)
#         # print(patch_index)
#         # print(np.sum(np.sum(np.sum(actual_mask_patch, axis = 1), axis =1)))
#         patch_index = patch_index[np.newaxis,:]
# 
#         if np.sum(np.sum(np.sum(actual_mask_patch, axis = 1), axis =1)) > 0:
#             positive_patches = np.append(positive_patches, patch_index, axis=0)
#             print("Positive patch found" + str(patch_index))


    



def filter_positive_patches_in_scan(positive_patches, mask, scan_num):
    indexek = compute_patch_indices((512, 512, 512), (64, 64, 64), 32, 0)
    patch_counter = positive_patches.shape[0]
    for patch_index in indexek:
        actual_mask_patch = get_patch_from_mask_array(mask, (64,64,64), patch_index)
        patch_index = np.append(scan_num, patch_index)
        # print(patch_index)
        # print(np.sum(np.sum(np.sum(actual_mask_patch, axis = 1), axis =1)))
        patch_index = patch_index[np.newaxis,:]

        if np.sum(np.sum(np.sum(actual_mask_patch, axis = 1), axis =1)) > 0:
            positive_patches = np.append(positive_patches, patch_index, axis=0)
            print("Positive patch found" + str(patch_index))
    print("Found " + str(positive_patches.shape[0] - patch_counter) + " positive patches in scan " + str(scan_num) + " out of " + str(indexek.shape[0]))

    return positive_patches





def fix_array(positive_patches):
    positive_patches_sum = np.ndarray(shape=(0, 4), dtype=np.uint16)

    for patch in positive_patches:
        positive_patches_sum = np.append(positive_patches_sum, patch, axis=0)


    return positive_patches_sum


def joblib_loop(positive_patches, mask, scan_num):
    positive_patches_in_scan = filter_positive_patches_in_scan(positive_patches, mask, scan_num)
    return positive_patches_in_scan


def filter_patches(zarr_mask_array):
    
    positive_patches_arr = Parallel(n_jobs=-1)(delayed(joblib_loop)(positive_patches, mask, scan_num) for scan_num, mask in enumerate(zarr_mask_array))

    return np.array(positive_patches_arr)



# def joblib_loop_(slice):
#     # _, _, _, _, _, _, _, current_segmented_slice = segment_lung_from_slice(slice)
#     return current_segmented_slice

# def segment_lung_from_scan(scan):
# 
# 
#     segmented_lung_array = Parallel(n_jobs=-1)(delayed(joblib_loop)(slice) for slice in scan)
# 
#     return np.array(segmented_lung_array)





store = zarr.DirectoryStore(os.getcwd() + "/zarrTrainingDataset.zarr")


zarr_training_group_name = 'training'
zarr_validation_group_name = 'validation'
zarr_scan_array_name = 'scans'
zarr_mask_array_name = 'masks'
root = zarr.open_group(store=store, synchronizer=zarr.ThreadSynchronizer(), mode='r')
zarr_scan_array = root[os.sep.join([zarr_training_group_name, zarr_scan_array_name])]
zarr_mask_array = root[os.sep.join([zarr_training_group_name, zarr_mask_array_name])]

indexek = compute_patch_indices((512,512,512),(64,64,64),32,0)
positive_patches = np.ndarray(shape=(0, 4), dtype=np.uint16)

positive_patches = fix_array(filter_patches(zarr_mask_array))
np.savetxt('data_int.csv', positive_patches.tolist(), delimiter=',', fmt='%i')
data = {"nodule_patch_indices": positive_patches.tolist()}
with open('../Training/positive_indices.json', 'w') as file:
    json.dump(data, file)