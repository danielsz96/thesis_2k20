import pylidc as pl
import scipy
import os
from os import listdir
import SimpleITK as sitk
import numpy as np
from pylidc.utils import consensus
from skimage.measure import find_contours
import matplotlib.pyplot as plt



def dir_filename_scanner(luna_path=luna_path):
    series_instance_uid_list = []
    for names in listdir(luna_path):
        if (names.endswith(".mhd") and not names.endswith("_mask.mhd")):
            series_instance_uid_list.append(os.path.splitext(names)[0])
    return series_instance_uid_list






def load_ct_mhd(luna_path, series_instance_uid):
    img_file = luna_path + '/' + series_instance_uid + ".mhd"
    itk_img = sitk.ReadImage(img_file)
    npArray_imgs = sitk.GetArrayViewFromImage(itk_img)
    return npArray_imgs




def luna_scans_toarray_resize(luna_path,resize_factor = 0.5):
    series_instance_uid_list = dir_filename_scanner(luna_path)

    imgs_array = load_ct_mhd(luna_path, series_instance_uid_list[0])
    imgs_array = scipy.ndimage.zoom(imgs_array, (1, resize_factor, resize_factor), mode='nearest')


    for i,series_instance_uid in enumerate(series_instance_uid_list):
        if not series_instance_uid == series_instance_uid_list[0]:
            img_array = load_ct_mhd(luna_path, series_instance_uid)
            img_array = scipy.ndimage.zoom(img_array, (1, resize_factor, resize_factor), mode='nearest')

            imgs_array = np.concatenate((imgs_array,img_array))
            print(i+1, "scans added")

    print(i+1 ,"scan(s) loaded and resized by" , resize_factor)
    return imgs_array


def get_annotation(img_array, series_instance_uid):
    scan = pl.query(pl.Scan).filter(pl.Scan.series_instance_uid == series_instance_uid).first()
    nods = scan.cluster_annotations()
    num_z, height, width = img_array.shape
    luna_mask = np.full([num_z, height, width], fill_value=0, dtype = np.uint8)
    for anns in nods:

        cmask,cbbox,_ = consensus(anns)
        z_min = cbbox[2].start
        z_max = cbbox[2].stop
        x_min = cbbox[1].start
        x_max = cbbox[1].stop
        y_min = cbbox[0].start
        y_max = cbbox[0].stop

        cmask = cmask.astype(float)
        cmask = np.transpose(cmask)
        cmask = np.swapaxes(cmask,1,2)

        luna_mask_current = luna_mask[z_min:z_max,y_min:y_max,x_min:x_max]
        luna_mask[z_min:z_max,y_min:y_max,x_min:x_max] = np.maximum(cmask,luna_mask_current)

    return luna_mask

ct_array = luna_scans_toarray_resize(luna_path, 1)

print(ct_array.shape)





def get_all_annotations_toarray(luna_path, resize_factor = 0.5):

    uid_list = dir_filename_scanner(luna_path)
    mask = load_ct_mhd(luna_path, uid_list[0])
    num_z, height, width = mask.shape
    luna_masks_array = np.full([num_z, height, width], fill_value=0, dtype = np.uint8)
    luna_masks_array = scipy.ndimage.zoom(luna_masks_array, (1, resize_factor, resize_factor), mode='nearest')
    for i, series_instance_uid in enumerate(uid_list):


        actual = load_ct_mhd(luna_path, series_instance_uid)
        scan = pl.query(pl.Scan).filter(pl.Scan.series_instance_uid == series_instance_uid).first()
        nods = scan.cluster_annotations()
        num_z, height, width = actual.shape
        luna_mask = np.full([num_z, height, width], fill_value=0, dtype = np.uint8)

        for anns in nods:
            # Each object here corresponds to a nodule with multiple annotations
            # Perform a consensus consolidation and 50% agreement level
            # Padding the slices to add context for viewing
            cmask,cbbox,_ = consensus(anns)
            # Bounding box boundary coordinates
            z_min = cbbox[2].start
            z_max = cbbox[2].stop
            x_min = cbbox[1].start
            x_max = cbbox[1].stop
            y_min = cbbox[0].start
            y_max = cbbox[0].stop
            # The consensus function returns the consensus mask as a bool array
            # The following row changes it to numeric values
            cmask = cmask.astype(float)
            # Change coordinates to z, y, x order
            cmask = np.transpose(cmask)
            cmask = np.swapaxes(cmask,1,2)
            # Updating luna_mask array with the mask of the current nodule
            # If there are multiple nodules, the luna_mask array
            # gets updated
            luna_mask_current = luna_mask[z_min:z_max,y_min:y_max,x_min:x_max]
            luna_mask[z_min:z_max,y_min:y_max,x_min:x_max] = np.maximum(cmask,luna_mask_current)


        if i==0:
            luna_masks_array = luna_mask = scipy.ndimage.zoom(luna_mask, (1, resize_factor, resize_factor), mode='nearest')

        else :
            luna_mask = scipy.ndimage.zoom(luna_mask, (1, resize_factor, resize_factor), mode='nearest')

            luna_masks_array = np.concatenate((luna_masks_array,luna_mask))


    print(i+1 ,"masks loaded and refactored by", resize_factor)




    return luna_masks_array








