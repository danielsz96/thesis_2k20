import zarr
import numpy as np
import os
import matplotlib.pyplot as plt
store = zarr.DirectoryStore(os.getcwd() + "/zarrTrainingDataset.zarr")


zarr_training_group_name = 'training'
zarr_validation_group_name = 'validation'
zarr_scan_array_name = 'scans'
zarr_mask_array_name = 'masks'
root = zarr.open_group(store=store, synchronizer=zarr.ThreadSynchronizer(), mode='r')
zarr_scan_array = root[os.sep.join([zarr_training_group_name, zarr_scan_array_name])]
zarr_mask_array = root[os.sep.join([zarr_training_group_name, zarr_mask_array_name])]

print(zarr_mask_array[0].shape)
print(zarr_mask_array[1].shape)
print(np.unique(zarr_mask_array[0]))
for scan_num, scan in enumerate(zarr_scan_array):
    figure, axes = plt.subplots(1, 2)

    axes[0].imshow(scan[256], cmap='viridis')
    axes[0].set_title('scan', fontsize=8)

    axes[1].imshow(zarr_mask_array[scan_num][256], cmap='binary')
    axes[1].set_title('mask', fontsize=8)
    figure.tight_layout()
    # plt.show()
    plt.savefig("/Users/szabodaniel/Thesis/thesis_2k20/PreProcess/scan_number_"+ str(scan_num) + '.pdf')
    plt.close('all')

