

import numpy as np  # linear algebra

import os
import scipy.ndimage as ndimage
import matplotlib.pyplot as plt

from skimage import measure, morphology, segmentation
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

def generate_markers(image):
    marker_internal = image < -400
    marker_internal = segmentation.clear_border(marker_internal)
    marker_internal_labels = measure.label(marker_internal)
    areas = [r.area for r in measure.regionprops(marker_internal_labels)]
    areas.sort()
    if len(areas) > 2:
        for region in measure.regionprops(marker_internal_labels):
            if region.area < areas[-2]:
                for coordinates in region.coords:
                    marker_internal_labels[coordinates[0], coordinates[1]] = 0
    marker_internal = marker_internal_labels > 0
    external_a = ndimage.binary_dilation(marker_internal, iterations=10)
    external_b = ndimage.binary_dilation(marker_internal, iterations=55)
    marker_external = external_b ^ external_a
    marker_watershed = np.zeros((512, 512), dtype=np.int)
    marker_watershed += marker_internal * 255
    marker_watershed += marker_external * 128

    return marker_internal, marker_external, marker_watershed



def seperate_lungs(image):
    marker_internal, marker_external, marker_watershed = generate_markers(image)

    sobel_filtered_dx = ndimage.sobel(image, 1)
    sobel_filtered_dy = ndimage.sobel(image, 0)
    sobel_gradient = np.hypot(sobel_filtered_dx, sobel_filtered_dy)
    sobel_gradient *= 255.0 / np.max(sobel_gradient)

    watershed = morphology.watershed(sobel_gradient, marker_watershed)

    outline = ndimage.morphological_gradient(watershed, size=(3, 3))
    outline = outline.astype(bool)

    blackhat_struct = [[0, 0, 1, 1, 1, 0, 0],
                       [0, 1, 1, 1, 1, 1, 0],
                       [1, 1, 1, 1, 1, 1, 1],
                       [1, 1, 1, 1, 1, 1, 1],
                       [1, 1, 1, 1, 1, 1, 1],
                       [0, 1, 1, 1, 1, 1, 0],
                       [0, 0, 1, 1, 1, 0, 0]]





    blackhat_struct = ndimage.iterate_structure(blackhat_struct, 8)
    outline += ndimage.black_tophat(outline, structure=blackhat_struct)

    lungfilter = np.bitwise_or(marker_internal, outline)
    lungfilter = ndimage.morphology.binary_closing(lungfilter, structure=np.ones((5, 5)), iterations=3)

    segmented = np.where(lungfilter == 1, image, -2000 * np.ones((512, 512)))

    return segmented, lungfilter, outline, watershed, sobel_gradient, marker_internal, marker_external, marker_watershed


# Some Testcode:
# test_segmented, test_lungfilter, test_outline, test_watershed, test_sobel_gradient, test_marker_internal, test_marker_external, test_marker_watershed = seperate_lungs(
#     x_data[3])

# print("Sobel Gradient")
# plt.figure(1)
# plt.imshow(test_sobel_gradient, cmap='gray')
# plt.show()
# print("Watershed Image")
# plt.figure(2)
# plt.imshow(test_watershed, cmap='gray')
# plt.show()
# plt.figure(3)
# print("Outline after reinclusion")
# plt.imshow(test_outline, cmap='gray')
# plt.show()
# plt.figure(4)
# print("Lungfilter after closing")
# plt.imshow(test_lungfilter, cmap='gray')
# plt.show()
# plt.figure(5)
# print("Segmented Lung")
# plt.imshow(test_segmented, cmap='gray')
# plt.show()