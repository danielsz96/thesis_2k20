import copy

from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import disk, dilation, binary_erosion, binary_closing
from skimage.filters import roberts, sobel
from skimage import measure, morphology
import numpy as np
from scipy import ndimage as ndi


def segment_lungs(x_data):
    segmented_x_data = np.ndarray(shape=(0, x_data.shape[1], x_data.shape[2]), dtype='int16')
    for i, im in enumerate(x_data):

        # Step 1: Convert into a binary image.
        binary = im < -400

        # Step 2: Remove the blobs connected to the border of the image.
        cleared = clear_border(binary)
        # Step 3: Label the image.
        label_image = label(cleared)

        # Step 4: Keep the labels with 2 largest areas.
        areas = [r.area for r in regionprops(label_image)]
        areas.sort()
        if len(areas) > 2:
            for region in regionprops(label_image):
                if region.area < areas[-2]:
                    for coordinates in region.coords:
                        label_image[coordinates[0], coordinates[1]] = 0
        binary = label_image > 0

        # Step 5: Erosion operation with a disk of radius 2. This operation is seperate the lung nodules attached to the blood vessels.
        selem = disk(2)
        binary = binary_erosion(binary, selem)
        # Step 6: Closure operation with a disk of radius 10. This operation is    to keep nodules attached to the lung wall.
        selem = disk(10)  # CHANGE BACK TO 10
        binary = binary_closing(binary, selem)
        # Step 7: Fill in the small holes inside the binary mask of lungs.
        edges = roberts(binary)
        binary = ndi.binary_fill_holes(edges)
        # Step 8: Superimpose the binary mask on the input image.
        get_high_vals = binary == 0
        im[get_high_vals] = -2000
        segmented_x_data = np.append(segmented_x_data, im[np.newaxis, :, :], axis=0)

    print("Segmneted lung pics:", i + 1)
    return segmented_x_data

def segment_lung_single(x_data):
    # segmented_x_data = np.ndarray(shape=(0, x_data.shape[1], x_data.shape[2]), dtype='int16')
    im = copy.deepcopy(x_data)

    # Step 1: Convert into a binary image.
    binary = im < -400

    # Step 2: Remove the blobs connected to the border of the image.
    cleared = clear_border(binary)
    # Step 3: Label the image.
    label_image = label(cleared)

    # Step 4: Keep the labels with 2 largest areas.
    areas = [r.area for r in regionprops(label_image)]
    areas.sort()
    if len(areas) > 2:
        for region in regionprops(label_image):
            if region.area < areas[-2]:
                for coordinates in region.coords:
                    label_image[coordinates[0], coordinates[1]] = 0
    binary = label_image > 0

    # Step 5: Erosion operation with a disk of radius 2. This operation is seperate the lung nodules attached to the blood vessels.
    selem = disk(2)
    binary = binary_erosion(binary, selem)
    # Step 6: Closure operation with a disk of radius 10. This operation is    to keep nodules attached to the lung wall.
    selem = disk(10)  # CHANGE BACK TO 10
    binary = binary_closing(binary, selem)
    # Step 7: Fill in the small holes inside the binary mask of lungs.
    edges = roberts(binary)
    binary = ndi.binary_fill_holes(edges)
    # Step 8: Superimpose the binary mask on the input image.
    get_high_vals = binary == 0
    im[get_high_vals] = -2000
    # segmented_x_data = np.append(segmented_x_data, im[np.newaxis, :, :], axis=0)

    # print("Segmneted lung pics:", i + 1)
    return im
