from numcodecs import Blosc
import zarr
import numpy as np
import os
import matplotlib.pyplot as plt
luna_path = str(os.getcwd())

zarr_dir = os.getcwd() + "/Zarr dataset"

compressor = Blosc(cname='zstd', clevel=3, shuffle=Blosc.BITSHUFFLE)

store = zarr.DirectoryStore(zarr_dir)
training_data = np.load('compressed_test_data.npz.npz')
x_data = training_data.f.images
y_data = training_data.f.masks




def to_zarr(data_to_zarr):
    if not os.path.exists(zarr_dir): mode = "new_array"
    else: mode = "extisting_array"




    if mode == "new_array":
        os.makedirs(zarr_dir)
        print(mode)
        new_array = zarr.creation.empty((0,256,256), store = store, compressor = compressor, chunks=(100,256, 256))
        print(new_array.info)
        new_array.append(data_to_zarr)

    else:
        print(mode)
        zarray = zarr.convenience.open(store = store, mode='r+')
        before = zarray.shape[0]
        zarray.append(data_to_zarr)
        counter = zarray.shape[0] - before
        print(str(counter) + "  images appended to zarr, sum: " + str(zarray.shape[0]))
        print(zarray.info)
        # img = zarray[222]
        # print(type(img))
        # plt.figure(1)
        # plt.imshow(img)
        # plt.show()



def zarr_open():
    zarray = zarr.convenience.open(store=store, mode='r+')
    print(zarray.info)
    print(zarray.shape)
    img = zarray[435]
    print(type(img))
    plt.figure(1)
    plt.imshow(img)
    plt.show()













