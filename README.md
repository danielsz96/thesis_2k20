# Thesis_2k20

Object detection with deep learning on medical images

Medical imaging procedures such as CT (Computer Tomography) have played an important role in theaccurate diagnosis of certain disease groups for decades.The topic of the thesis is the development of a decision support system for lung CT scans, by which cancerous lesions and nodules in the lung can be identified. In recent years, methods based on artificialintelligence have become dominant in object detection. The student is tasked with training a neural network for the task. Lung nodules can be said to be scattered and sparse in the volume of the lung, so the error function used for training largely determines the sensitivity and specificity of the solution, so the main task is to select and optimize it.