
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf




import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    print("Name:", gpu.name, "  Type:", gpu.device_type)

from tensorflow.python.client import device_lib

device_lib.list_local_devices()

tf.test.is_gpu_available()

mnist = tf.keras.datasets.mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()

x_train, x_test = x_train / 255.0, x_test / 255.0

# %% [markdown]
# # Create Sequential Model Using Tensorflow Keras
#
# Architecture of the Network is :-
#
# 1). Input layer for 28x28 images in MNiST dataset
#
# 2). Dense layer with 128 neurons and ReLU activation function
#
# 3). Output layer with 10 neurons for classification of input images as one of ten digits(0 to 9)



# %% [code]


# %% [markdown]
# # Creating Loss Function

# %% [code]
loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

# %% [markdown]
# # Compile the Model Designed Earlier
#
# Before the model is ready for training, it needs a few more settings. These are added during the model's compile step:
#
# - Loss function
# This measures how accurate the model is during training. You want to minimize this function to "steer" the model in the right direction.
#
# - Optimizer
# This is how the model is updated based on the data it sees and its loss function.
#
# - Metrics
# Used to monitor the training and testing steps. The following example uses accuracy, the fraction of the images that are correctly classified.

# %% [code]



strategy = tf.distribute.MirroredStrategy()

with strategy.scope():


    model = tf.keras.models.Sequential([
      tf.keras.layers.Flatten(input_shape=(28, 28)),
      tf.keras.layers.Dense(128, activation='relu'),
      tf.keras.layers.Dropout(0.2),
      tf.keras.layers.Dense(10)
    ])


predictions = model(x_train[:1]).numpy()
predictions


model.compile(optimizer='adam',
              loss=loss_fn,
              metrics=['accuracy'])



model.fit(x_train, y_train, epochs=30, batch_size=32)

# The Model.evaluate method checks the models performance, usually on a "Validation-set" or "Test-set".

model.evaluate(x_test,  y_test, verbose=1)
