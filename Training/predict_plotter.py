import math
import keras
import tensorflow as tf
import numpy as np
import json

def dsc(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return score

def dice_loss(y_true, y_pred):
    loss = 1 - dsc(y_true, y_pred)
    return loss

def bce_dice_loss(y_true, y_pred):
    loss = binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    return loss

def confusion(y_true, y_pred):
    smooth=1
    y_pred_pos = K.clip(y_pred, 0, 1)
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.clip(y_true, 0, 1)
    y_neg = 1 - y_pos
    tp = K.sum(y_pos * y_pred_pos)
    fp = K.sum(y_neg * y_pred_pos)
    fn = K.sum(y_pos * y_pred_neg)
    prec = (tp + smooth)/(tp+fp+smooth)
    recall = (tp+smooth)/(tp+fn+smooth)
    return prec, recall

def tp(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pos = K.round(K.clip(y_true, 0, 1))
    tp = (K.sum(y_pos * y_pred_pos) + smooth)/ (K.sum(y_pos) + smooth)
    return tp

def tn(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.round(K.clip(y_true, 0, 1))
    y_neg = 1 - y_pos
    tn = (K.sum(y_neg * y_pred_neg) + smooth) / (K.sum(y_neg) + smooth )
    return tn

def tversky(y_true, y_pred):
    y_true_pos = K.flatten(y_true)
    y_pred_pos = K.flatten(y_pred)
    true_pos = K.sum(y_true_pos * y_pred_pos)
    false_neg = K.sum(y_true_pos * (1-y_pred_pos))
    false_pos = K.sum((1-y_true_pos)*y_pred_pos)
    alpha = 0.7
    return (true_pos + smooth)/(true_pos + alpha*false_neg + (1-alpha)*false_pos + smooth)

def tversky_loss(y_true, y_pred):
    return 1 - tversky(y_true,y_pred)

def focal_tversky(y_true,y_pred):
    pt_1 = tversky(y_true, y_pred)
    gamma = 0.75
    return K.pow((1-pt_1), gamma)



from random import shuffle

def split_list(input_list, split=0.8, shuffle_list=False):
    if shuffle_list:
        shuffle(input_list)
    n_training = int(len(input_list) * split)
    training = input_list[:n_training]
    testing = input_list[n_training:]
    return training, testing

#data load functions--------------------------------------------------------------------
def pickle_dump(item, out_file):
    with open(out_file, "wb") as opened_file:
        pickle.dump(item, opened_file)


def pickle_load(in_file):
    with open(in_file, "rb") as opened_file:
        return pickle.load(opened_file)


def normalize(image, min_bound=-1000, max_bound=400):

    image = (image - min_bound) / (max_bound - min_bound)
    image[image > 1] = 1.
    image[image < 0] = 0.

    return image

def get_patch_from_4d_data(scan_array, mask_array, patch_shape, patch_index):

    scan_patch = get_patch_from_3d_data(scan_array[patch_index[0]], patch_shape, patch_index[1:])
    mask_patch = get_patch_from_3d_data(mask_array[patch_index[0]], patch_shape, patch_index[1:])

    return scan_patch, mask_patch


def fix_out_of_bound_patch_attempt(data, patch_shape, patch_index, ndim=3):

    image_shape = data.shape[-ndim:]
    pad_before = np.abs((patch_index < 0) * patch_index)
    pad_after = np.abs(((patch_index + patch_shape) > image_shape) * ((patch_index + patch_shape) - image_shape))
    pad_args = np.stack([pad_before, pad_after], axis=1)
    if pad_args.shape[0] < len(data.shape):
        pad_args = [[0, 0]] * (len(data.shape) - pad_args.shape[0]) + pad_args.tolist()
    data = np.pad(data, pad_args, mode="edge")
    patch_index += pad_before
    return data, patch_index

def get_patch_from_3d_data(data, patch_shape, patch_index):

    patch_index = np.asarray(patch_index, dtype=np.int16)
    patch_shape = np.asarray(patch_shape)
    image_shape = data.shape[-3:]
    if np.any(patch_index < 0) or np.any((patch_index + patch_shape) > image_shape):
        data, patch_index = fix_out_of_bound_patch_attempt(data, patch_shape, patch_index)
    return data[..., patch_index[0]:patch_index[0]+patch_shape[0], patch_index[1]:patch_index[1]+patch_shape[1],
                patch_index[2]:patch_index[2]+patch_shape[2]]





model = keras.models.load_model("dice_16overlap20201208-155726.h5", custom_objects={'dice_loss' : dice_loss,
                                                                                  'dsc': dsc,
                                                                                  'bin_acc' : tf.keras.metrics.BinaryAccuracy(name="binary_accuracy", dtype=None, threshold=0.5)})



scan_array = np.load('scans.npy', mmap_mode="r")
mask_array = np.load('masks.npy', mmap_mode="r")


with open('positive_indices_16.json', 'r') as j:
    json_data = json.load(j)

index_list= json_data['nodule_patch_indices']


train_indices, val_indices = split_list(index_list)
shuffle(train_indices)
shuffle(val_indices)


train_steps_per_epoch = (math.floor(len(train_indices) / 16))
val_steps_per_epoch = (math.floor(len(val_indices) / 16))




to_be_predicted = list()
masks = list()

for val_index in val_indices[20:]:

    x = normalize(get_patch_from_3d_data(scan_array[val_index[0]], (64,64,64), val_index[1:]))
    y = get_patch_from_3d_data(mask_array[val_index[0]], (64,64,64), val_index[1:])
    x = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)
    y = y.reshape(y.shape[0], y.shape[1], y.shape[2], 1)

    to_be_predicted.append(x)
    masks.append(y)


scans = np.asarray(to_be_predicted)
masks = np.asarray(masks)




prediction = model.predict(scans)
preds = prediction



from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure, morphology
import matplotlib.pyplot as plt
import numpy as np






def save_plot_3d(image, filename,  threshold=.3):
    p = image.transpose(2,1,0)
    verts, faces, normals, values = measure.marching_cubes(p, threshold)


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha=0.3)
    face_color = [0.5, 0.5, 1]
    mesh.set_facecolor(face_color)
    ax.add_collection3d(mesh)
    ax.set_xlim(0, p.shape[0])
    ax.set_ylim(0, p.shape[1])
    ax.set_zlim(0, p.shape[2])
    plt.savefig("3dplot_" + str(filename) + ".pdf")






for i in range(preds.shape[0]):
    save_plot_3d(np.squeeze(preds[i]), str(i) + "pred")
    save_plot_3d(np.squeeze(scans[i]), str(i) + "scan")
    save_plot_3d(np.squeeze(masks[i]), str(i) + "mask")
