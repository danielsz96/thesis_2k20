import os
from random import shuffle
import numpy as np
import pickle
import copy
import matplotlib.pyplot as plt
plt.switch_backend('Agg')


def pickle_dump(item, out_file):
    with open(out_file, "wb") as opened_file:
        pickle.dump(item, opened_file)


def pickle_load(in_file):
    with open(in_file, "rb") as opened_file:
        return pickle.load(opened_file)


def get_training_and_validation_generators(scan_array, mask_array, index_list,  batch_size, training_keys_file, validation_keys_file,
                                           data_split=0.8, overwrite=False, augment=False,
                                           patch_shape=None,
                                           validation_batch_size=False, shuffle_index_list=True, skip_blank=True):

    if not validation_batch_size:
        validation_batch_size = batch_size

    training_list, validation_list = get_validation_split(index_list,
                                                          data_split=data_split,
                                                          overwrite=overwrite,
                                                          training_file=training_keys_file,
                                                          validation_file=validation_keys_file)
    print("Training list lenght:  " + str(len(training_list)))
    print("Validation list lenght:  " + str(len(validation_list)))
    training_generator = data_generator("training_generator", scan_array, mask_array, training_list,
                                        batch_size=batch_size,
                                        augment=augment,
                                        patch_shape=patch_shape,
                                        shuffle_index_list=shuffle_index_list)

    validation_generator = data_generator("validation_generator",scan_array, mask_array, validation_list,
                                          batch_size=validation_batch_size,
                                          patch_shape=patch_shape,
                                          shuffle_index_list=shuffle_index_list)


    num_training_steps = get_number_of_steps(len(training_list), batch_size)
    print("Calculated of training steps per epoch: ", num_training_steps)

    num_validation_steps = get_number_of_steps(len(validation_list), validation_batch_size)
    print("Calculated of validation steps per epoch: ", num_validation_steps)

    return training_generator, validation_generator, num_training_steps, num_validation_steps


def get_number_of_steps(n_samples, batch_size):
    if n_samples <= batch_size:
        return n_samples
    elif np.remainder(n_samples, batch_size) == 0:
        return n_samples//batch_size
    else:
        return n_samples//batch_size + 1


def split_list(input_list, split=0.8, shuffle_list=True):
    if shuffle_list:
        shuffle(input_list)
    n_training = int(len(input_list) * split)
    training = input_list[:n_training]
    testing = input_list[n_training:]
    return training, testing

def get_validation_split(index_list, training_file, validation_file, data_split=0.8, overwrite=False):

    if overwrite or not os.path.exists(training_file):
        print("Creating validation split...")

        training_list, validation_list = split_list(index_list, split=data_split)
        pickle_dump(training_list, training_file)
        pickle_dump(validation_list, validation_file)
        return training_list, validation_list
    else:
        print("Loading previous validation split...")
        return pickle_load(training_file), pickle_load(validation_file)



def data_generator(whatgenerator, scan_array, mask_array, index_list_original, batch_size, augment=False, patch_shape=None,
                   shuffle_index_list=True):
    epoch = 0
    while True:
        x_list = list()
        y_list = list()

        i = 1
        index_list = copy.copy(index_list_original)

        if shuffle_index_list:
            shuffle(index_list)

        while len(index_list) > 0:
            index = index_list.pop()
            if i == 1 and epoch == 0:
                index_list = copy.copy(index_list_original)

            add_data(x_list, y_list, scan_array, mask_array, index, augment=augment, patch_shape=patch_shape)
            if len(x_list) == batch_size or (len(index_list) == 0 and len(x_list) > 0):
                batch_x = np.asarray(x_list)
                batch_y = np.asarray(y_list)
                batch_x = batch_x.reshape(batch_x.shape[0], batch_x.shape[1], batch_x.shape[2], batch_x.shape[3], 1)
                batch_y = batch_y.reshape(batch_y.shape[0], batch_y.shape[1], batch_y.shape[2], batch_y.shape[3], 1)

                # figure, axes = plt.subplots(1, 2)
                #
                # axes[0].imshow(batch_x[0][16], cmap='viridis')
                # axes[0].set_title('scan', fontsize=8)
                #
                # axes[1].imshow(batch_y[0][16], cmap='binary')
                # axes[1].set_title('mask', fontsize=8)
                # figure.tight_layout()
                # # print("\nPrepared batch " + str(i) +" for "+ whatgenerator  +" and saved test in epoch " + str(epoch) )
                #
                # plt.savefig(whatgenerator + str(i) + '.pdf')
                # plt.close('all')

                i = i + 1

                yield batch_x, batch_y

                if epoch == 0:
                    i = 1
                    epoch = epoch + 1


                x_list = list()
                y_list = list()




def fix_out_of_bound_patch_attempt(data, patch_shape, patch_index, ndim=3):

    image_shape = data.shape[-ndim:]
    pad_before = np.abs((patch_index < 0) * patch_index)
    pad_after = np.abs(((patch_index + patch_shape) > image_shape) * ((patch_index + patch_shape) - image_shape))
    pad_args = np.stack([pad_before, pad_after], axis=1)
    if pad_args.shape[0] < len(data.shape):
        pad_args = [[0, 0]] * (len(data.shape) - pad_args.shape[0]) + pad_args.tolist()
    data = np.pad(data, pad_args, mode="edge")
    patch_index += pad_before
    return data, patch_index



def get_patch_from_4d_data(scan_array, mask_array, patch_shape, patch_index):

    scan_patch = get_patch_from_3d_data(scan_array[patch_index[0]], patch_shape, patch_index[1:])
    mask_patch = get_patch_from_3d_data(mask_array[patch_index[0]], patch_shape, patch_index[1:])

    return scan_patch, mask_patch


def get_patch_from_3d_data(data, patch_shape, patch_index):

    patch_index = np.asarray(patch_index, dtype=np.int16)
    patch_shape = np.asarray(patch_shape)
    image_shape = data.shape[-3:]
    if np.any(patch_index < 0) or np.any((patch_index + patch_shape) > image_shape):
        data, patch_index = fix_out_of_bound_patch_attempt(data, patch_shape, patch_index)
    return data[..., patch_index[0]:patch_index[0]+patch_shape[0], patch_index[1]:patch_index[1]+patch_shape[1],
                patch_index[2]:patch_index[2]+patch_shape[2]]


def add_data(x_list, y_list, scan_array, mask_array, index, augment, patch_shape):

    data, truth = get_patch_from_4d_data(scan_array, mask_array, patch_shape, index)
    # TODO: insert augmentor here
    x_list.append(data)
    y_list.append(truth.astype('float32'))
