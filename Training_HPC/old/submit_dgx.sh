#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p gpu2
#SBATCH --gres=gpu:v10032g:2	# select 2 V100s GPU
#SBATCH -o training_bin/gpu/dgx_train_outfile-%j.txt	# send stdout to outfile
#SBATCH -e training_bin/gpu/dgx_train_errfile-%j.txt		# send stderr to errfile
#SBATCH -J unet_training

source /home/szabda/env/bin/activate
module load python/3.6
module load gpu/cuda/10.1 gpu/cudnn/7.6.5-cuda10.1

python3 training_bin/gpu/training_main.py
