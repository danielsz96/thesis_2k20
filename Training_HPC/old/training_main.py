import os
import json
import tensorflow as tf
from generator import get_training_and_validation_generators
from unet_model import unet_model_3d
from training import train_model
from keras.models import load_model
from datetime import datetime


strategy = tf.distribute.MirroredStrategy()
print('Number of devices: {}'.format(strategy.num_replicas_in_sync))
print('Used gpus:   ' + str(len(tf.config.experimental.list_physical_devices('GPU'))))


import zarr
# store = zarr.DirectoryStore(os.getcwd() + "/zarrTrainingDataset.zarr")
# zarr_training_group_name = 'training'
# zarr_scan_array_name = 'scans'
# zarr_mask_array_name = 'masks'
# root = zarr.open_group(store=store, synchronizer=zarr.ThreadSynchronizer(), mode='r')
# zarr_scan_array = root[os.sep.join([zarr_training_group_name, zarr_scan_array_name])]
# zarr_mask_array = root[os.sep.join([zarr_training_group_name, zarr_mask_array_name])]


#
#
store = zarr.DirectoryStore("/home/szabda/training_bin/gpu/zarrTrainingDataset.zarr")
zarr_training_group_name = 'training'
zarr_scan_array_name = 'scans'
zarr_mask_array_name = 'masks'
root = zarr.open_group(store=store, synchronizer=zarr.ThreadSynchronizer(), mode='r')
zarr_scan_array = root[os.sep.join([zarr_training_group_name, zarr_scan_array_name])]
zarr_mask_array = root[os.sep.join([zarr_training_group_name, zarr_mask_array_name])]

with open('positive_indices.json', 'r') as j:
    json_data = json.load(j)

index_list_ = json_data['nodule_patch_indices']

#index_list_= index_list_[:1989]

# index_list_= index_list_[:4]

config = dict()
config["pool_size"] = (2, 2, 2)  # pool size for the max pooling operations
config["patch_shape"] = (64, 64, 64)
config["input_shape"] = (64,64,64,1)
config["deconvolution"] = False  # if False, will use upsampling instead of deconvolution

# config["batch_size"] = 6
# config["validation_batch_size"] = 12
config["batch_size"] =16
config["validation_batch_size"] = 12
config["n_epochs"] = 10
config["patience"] = 2  # learning rate will be reduced after this many epochs if the validation loss is not improving
config["early_stop"] = 2  # training will be stopped after this many epochs without the validation loss improving
config["initial_learning_rate"] = 1e-3
config["learning_rate_drop"] = 0.5  # factor by which the learning rate will be reduced
config["validation_split"] = 0.8  # portion of the data that will be used for training


config["model_file"] = os.path.abspath(str(datetime.now()) + "unet.h5")
config["training_file"] = ("/home/szabda/training_bin/training_ids.pkl")
config["validation_file"] = ("/home/szabda/training_bin/validation_ids.pkl")
config["overwrite"] = True  # If True, will previous files. If False, will use previously written files.


def main(overwrite=False):

    if not overwrite and os.path.exists(config["model_file"]):

        model = load_model(config["model_file"])
    else:
        model = unet_model_3d(input_shape=config["input_shape"],
                              pool_size=config["pool_size"],
                              n_labels=1,
                              initial_learning_rate=config["initial_learning_rate"],
                              deconvolution=config["deconvolution"])
        model.save(config["model_file"])


    train_generator, validation_generator, n_train_steps, n_validation_steps = get_training_and_validation_generators(
        zarr_scan_array,
        zarr_mask_array,
        index_list_,
        batch_size=config["batch_size"],
        validation_keys_file=config["validation_file"],
        training_keys_file=config["training_file"],
        data_split=config["validation_split"],
        overwrite=overwrite,
        augment=False,
        patch_shape=config["patch_shape"],
        validation_batch_size = 12,
        skip_blank = True)



    train_model(model=model,
                model_file=config["model_file"],
                training_generator=train_generator,
                validation_generator=validation_generator,
                steps_per_epoch=n_train_steps,
                validation_steps=n_validation_steps,
                initial_learning_rate=config["initial_learning_rate"],
                learning_rate_drop=config["learning_rate_drop"],
                learning_rate_patience=config["patience"],
                early_stopping_patience=config["early_stop"],
                n_epochs=config["n_epochs"])



with strategy.scope():

    if __name__ == "__main__":
         main(overwrite=config["overwrite"])
