#!/bin/bash
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -p gpu
#SBATCH --gres=gpu:v100:2	# select 2 V100 GPU
#SBATCH -o set1/_outfile-%j.txt		# send stdout to outfile
#SBATCH -e set1/_errfile-%j.txt		# send stderr to errfile
#SBATCH -J unet_training

cd
source /home/szabda/env/bin/activate
module load python/3.6
module load gpu/cuda/10.1 gpu/cudnn/7.6.5-cuda10.1
cd /home/szabda/set2
python dice_loss.py
