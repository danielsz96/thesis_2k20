import os
import  time
import keras
import random
import json
import tensorflow as tf
from keras.models import load_model
from datetime import datetime
import numpy as np
import tensorflow as tf
import math
from functools import partial
from keras.optimizers import Adam, SGD
from keras import backend as K
from keras.callbacks import ModelCheckpoint, CSVLogger, LearningRateScheduler, ReduceLROnPlateau, EarlyStopping
from keras.models import load_model

from keras.losses import binary_crossentropy

os.system("taskset -p 0xff %d" % os.getpid())


smooth = 1

def dsc(y_true, y_pred):
    smooth = 1.
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    score = (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
    return score

def dice_loss(y_true, y_pred):
    loss = 1 - dsc(y_true, y_pred)
    return loss

def bce_dice_loss(y_true, y_pred):
    loss = binary_crossentropy(y_true, y_pred) + dice_loss(y_true, y_pred)
    return loss

def confusion(y_true, y_pred):
    smooth=1
    y_pred_pos = K.clip(y_pred, 0, 1)
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.clip(y_true, 0, 1)
    y_neg = 1 - y_pos
    tp = K.sum(y_pos * y_pred_pos)
    fp = K.sum(y_neg * y_pred_pos)
    fn = K.sum(y_pos * y_pred_neg)
    prec = (tp + smooth)/(tp+fp+smooth)
    recall = (tp+smooth)/(tp+fn+smooth)
    return prec, recall



def tp(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pos = K.round(K.clip(y_true, 0, 1))
    tp = (K.sum(y_pos * y_pred_pos) + smooth)/ (K.sum(y_pos) + smooth)
    return tp

def tn(y_true, y_pred):
    smooth = 1
    y_pred_pos = K.round(K.clip(y_pred, 0, 1))
    y_pred_neg = 1 - y_pred_pos
    y_pos = K.round(K.clip(y_true, 0, 1))
    y_neg = 1 - y_pos
    tn = (K.sum(y_neg * y_pred_neg) + smooth) / (K.sum(y_neg) + smooth )
    return tn

def tversky(y_true, y_pred):
    y_true_pos = K.flatten(y_true)
    y_pred_pos = K.flatten(y_pred)
    true_pos = K.sum(y_true_pos * y_pred_pos)
    false_neg = K.sum(y_true_pos * (1-y_pred_pos))
    false_pos = K.sum((1-y_true_pos)*y_pred_pos)
    alpha = 0.7
    return (true_pos + smooth)/(true_pos + alpha*false_neg + (1-alpha)*false_pos + smooth)

def tversky_loss(y_true, y_pred):
    return 1 - tversky(y_true,y_pred)

def focal_tversky(y_true,y_pred):
    pt_1 = tversky(y_true, y_pred)
    gamma = 0.75
    return K.pow((1-pt_1), gamma)





#Config dict--------------------------------------------------------------------



config = dict()
config["train_id"] = "tversky_depth_5" + time.strftime("%Y%m%d-%H%M%S")


try:
    os.mkdir(config["train_id"])
except OSError:
    print ("Creation of the directory %s failed" % config["train_id"])
else:
    print ("Successfully created the directory %s " % config["train_id"])

config["workingdir"] = os.path.abspath(config["train_id"])
config["n_epochs"] = 50
config["pool_size"] = (2, 2, 2)  # pool size for the max pooling operations
config["input_shape"] = (64,64,64,1)
config["deconvolution"] = False  # if False, will use upsampling instead of deconvolution
config["model_file"] = config["workingdir"] + "/" + config["train_id"] + ".h5"
config["patience"] = 5  # learning rate will be reduced after this many epochs if the validation loss is not improving
config["early_stop"] = 20  # training will be stopped after this many epochs without the validation loss improving
config["initial_learning_rate"] = 1e-4
config["learning_rate_drop"] = 0.1  # factor by which the learning rate will be reduced
config["batch_size"] = 32
config["batch_norm"] = True


#losses-------------------------------------------------------------------------



#GPU test-----------------------------------------------------------------------


from tensorflow.python.client import device_lib
device_lib.list_local_devices()
tf.test.is_gpu_available()

#Load extracted training and validation data------------------------------------



#Set up model-------------------------------------------------------------------
import numpy as np
from keras import backend as K
from keras.engine import Input, Model
from keras.optimizers import Adam
from keras.layers import Conv3D, MaxPooling3D, UpSampling3D, Activation, BatchNormalization, PReLU
from keras.layers.convolutional import Deconvolution3D
import keras.backend as K
import tensorflow as tf
from keras.layers import Dropout



try:
    from keras.engine import merge
except ImportError:
    from keras.layers.merge import concatenate


def unet_model_3d(input_shape, pool_size=(2, 2, 2), n_labels=1, initial_learning_rate=0.0001, deconvolution=False,
                  depth=5, n_base_filters=32, batch_normalization=False, activation_name="sigmoid"):

    inputs = Input(input_shape)
    current_layer = inputs
    levels = list()


    # add levels with max pooling
    for layer_depth in range(depth):
        layer1 = create_convolution_block(input_layer=current_layer, n_filters=n_base_filters*(2**layer_depth),
                                          batch_normalization=False)

        layer1=Dropout(0.2)(layer1)
        layer2 = create_convolution_block(input_layer=layer1, n_filters=n_base_filters*(2**layer_depth)*2,
                                          batch_normalization=False)

        layer2=Dropout(0.2)(layer2)
        if layer_depth < depth - 1:
            current_layer = MaxPooling3D(pool_size=pool_size, data_format="channels_last")(layer2)
            levels.append([layer1, layer2, current_layer])
        else:
            current_layer = layer2
            levels.append([layer1, layer2])

    # add levels with up-convolution or up-sampling
    for layer_depth in range(depth-2, -1, -1):
        # print(current_layer.shape)
        up_convolution = get_up_convolution(pool_size=pool_size, deconvolution=deconvolution,
                                            n_filters=current_layer.shape[-1])(current_layer)


        concat = concatenate([up_convolution, levels[layer_depth][1]], axis=-1)
        current_layer = create_convolution_block(n_filters=levels[layer_depth][1].shape[1],
                                                 input_layer=concat, batch_normalization=batch_normalization)
        current_layer = create_convolution_block(n_filters=levels[layer_depth][1].shape[1],
                                                 input_layer=current_layer,
                                                 batch_normalization=batch_normalization)




    final_convolution = Conv3D(n_labels, (1, 1, 1), data_format="channels_last")(current_layer)
    act = Activation(activation_name)(final_convolution)
    model = Model(inputs=inputs, outputs=act)

    bin_acc = tf.keras.metrics.BinaryAccuracy(name="binary_accuracy", dtype=None, threshold=0.5)


    prec = tf.keras.metrics.Precision()
    recall = tf.keras.metrics.Recall()

    #model.compile(optimizer=SGD(lr=config["initial_learning_rate"], momentum=0.9), loss=config["loss"], metrics = [dsc, bin_acc, tp, tn])
    model.compile(optimizer=Adam(lr=config["initial_learning_rate"]), loss=config["loss"], metrics = [dsc, prec, recall])

    model.summary()
    return model


def create_convolution_block(input_layer, n_filters, batch_normalization=False, kernel=(3, 3, 3), activation=None,
                             padding='same', strides=(1, 1, 1), instance_normalization=True):


    layer = Conv3D(n_filters, kernel, padding=padding, strides=strides, data_format="channels_last")(input_layer)
    if batch_normalization:
        layer = BatchNormalization(axis=1)(layer)

    if activation is None:
        return Activation('relu')(layer)

    else:
        return activation()(layer)


def compute_level_output_shape(n_filters, depth, pool_size, image_shape):

    output_image_shape = np.asarray(np.divide(image_shape, np.power(pool_size, depth)), dtype=np.int32).tolist()
    return tuple([None, n_filters] + output_image_shape)


def get_up_convolution(n_filters, pool_size, kernel_size=(2, 2, 2), strides=(2, 2, 2),
                       deconvolution=False):
    if deconvolution:
        return Deconvolution3D(filters=n_filters, kernel_size=kernel_size,
                               strides=strides, data_format="channels_last")
    else:
        return UpSampling3D(size=pool_size, data_format="channels_last")



#Build model--------------------------------------------------------------------


strategy = tf.distribute.MirroredStrategy()

with strategy.scope():



    config["loss"] = tversky_loss
    model = unet_model_3d(input_shape=config["input_shape"],
                                  pool_size=config["pool_size"],
                                  n_labels=1,
                                  batch_normalization=config["batch_norm"],
                                  deconvolution=config["deconvolution"])





#Callsbacks---------------------------------------------------------------------



def get_callbacks(model_file, initial_learning_rate, learning_rate_drop,
                  learning_rate_patience, early_stopping_patience, verbosity=1):
    callbacks = list()
    callbacks.append(ModelCheckpoint(model_file, save_best_only=True, verbose=verbosity))
    callbacks.append(CSVLogger(config["workingdir"] + "/" + "traininglogs.csv", append=True))
    callbacks.append(tf.keras.callbacks.TensorBoard(log_dir=config["workingdir"] + "/" + "logs", histogram_freq=1, write_graph=True, write_images=True, update_freq='batch'))
    callbacks.append(ReduceLROnPlateau(factor=learning_rate_drop, patience=learning_rate_patience,verbose=verbosity,monitor='val_loss', min_lr=0, mode='min',min_delta=0.0001,))
    callbacks.append(EarlyStopping(verbose=verbosity, patience=early_stopping_patience))
    return callbacks







from random import shuffle

def split_list(input_list, split=0.8, shuffle_list=False):
    if shuffle_list:
        shuffle(input_list)
    n_training = int(len(input_list) * split)
    training = input_list[:n_training]
    testing = input_list[n_training:]
    return training, testing

#data load functions--------------------------------------------------------------------
def pickle_dump(item, out_file):
    with open(out_file, "wb") as opened_file:
        pickle.dump(item, opened_file)


def pickle_load(in_file):
    with open(in_file, "rb") as opened_file:
        return pickle.load(opened_file)


def normalize(image, min_bound=-1000, max_bound=400):

    image = (image - min_bound) / (max_bound - min_bound)
    image[image > 1] = 1.
    image[image < 0] = 0.

    return image

def get_patch_from_4d_data(scan_array, mask_array, patch_shape, patch_index):

    scan_patch = get_patch_from_3d_data(scan_array[patch_index[0]], patch_shape, patch_index[1:])
    mask_patch = get_patch_from_3d_data(mask_array[patch_index[0]], patch_shape, patch_index[1:])

    return scan_patch, mask_patch


def fix_out_of_bound_patch_attempt(data, patch_shape, patch_index, ndim=3):

    image_shape = data.shape[-ndim:]
    pad_before = np.abs((patch_index < 0) * patch_index)
    pad_after = np.abs(((patch_index + patch_shape) > image_shape) * ((patch_index + patch_shape) - image_shape))
    pad_args = np.stack([pad_before, pad_after], axis=1)
    if pad_args.shape[0] < len(data.shape):
        pad_args = [[0, 0]] * (len(data.shape) - pad_args.shape[0]) + pad_args.tolist()
    data = np.pad(data, pad_args, mode="edge")
    patch_index += pad_before
    return data, patch_index

def get_patch_from_3d_data(data, patch_shape, patch_index):

    patch_index = np.asarray(patch_index, dtype=np.int16)
    patch_shape = np.asarray(patch_shape)
    image_shape = data.shape[-3:]
    if np.any(patch_index < 0) or np.any((patch_index + patch_shape) > image_shape):
        data, patch_index = fix_out_of_bound_patch_attempt(data, patch_shape, patch_index)
    return data[..., patch_index[0]:patch_index[0]+patch_shape[0], patch_index[1]:patch_index[1]+patch_shape[1],
                patch_index[2]:patch_index[2]+patch_shape[2]]











# The Model.evaluate method checks the models performance, usually on a "Validation-set" or "Test-set".


def train_generator():
    for patch_index in train_indices:

        x = normalize(get_patch_from_3d_data(scan_array[patch_index[0]], (64,64,64), patch_index[1:]))
        y = get_patch_from_3d_data(mask_array[patch_index[0]], (64,64,64), patch_index[1:])
        axis_index = random.randint(0,2)
        x = np.flip(x, axis=axis_index)
        y = np.flip(y, axis=axis_index)
        x = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)
        y = y.reshape(y.shape[0], y.shape[1], y.shape[2], 1)

        yield x, y

def validation_generator():
    for val_index in val_indices:
 
        x = normalize(get_patch_from_3d_data(scan_array[val_index[0]], (64,64,64), val_index[1:]))
        y = get_patch_from_3d_data(mask_array[val_index[0]], (64,64,64), val_index[1:])
        x = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)
        y = y.reshape(y.shape[0], y.shape[1], y.shape[2], 1)
        yield x, y

train_dataset = tf.data.Dataset.from_generator(train_generator, (tf.float32, tf.float32))
train_dataset = train_dataset.batch(batch_size=config["batch_size"], drop_remainder=True)
train_dataset = train_dataset.repeat()

validation_dataset = tf.data.Dataset.from_generator(validation_generator, (tf.float32, tf.float32))
validation_dataset = validation_dataset.batch(batch_size=config["batch_size"], drop_remainder=True)
validation_dataset = validation_dataset.repeat()





scan_array = np.load('scans_full.npy', mmap_mode="r")
mask_array = np.load('masks_full.npy', mmap_mode="r")


with open('positive_indices_0_overlap.json', 'r') as j:
    json_data = json.load(j)

train_indices= json_data['nodule_patch_indices']


with open('validation.json', 'r') as j:
    json_data = json.load(j)

val_indices= json_data['nodule_patch_indices']

shuffle(train_indices)



train_steps_per_epoch = (math.floor(len(train_indices) / config["batch_size"]))
val_steps_per_epoch = (math.floor(len(val_indices) / config["batch_size"]))



model.fit(x = train_dataset,
            epochs=config["n_epochs"],
            validation_data=validation_dataset,
            steps_per_epoch=train_steps_per_epoch,
            validation_steps=val_steps_per_epoch,
            callbacks=get_callbacks(model_file = config["model_file"],
                                                 initial_learning_rate = config["initial_learning_rate"],
                                                 learning_rate_drop=config["learning_rate_drop"],
                                                 learning_rate_patience = config["patience"],
                                                 early_stopping_patience=config["early_stop"]))




to_be_predicted = list()
masks = list()



for val_index in val_indices[10:]:

    x = normalize(get_patch_from_3d_data(scan_array[val_index[0]], (64,64,64), val_index[1:]))
    y = get_patch_from_3d_data(mask_array[val_index[0]], (64,64,64), val_index[1:])
    x = x.reshape(x.shape[0], x.shape[1], x.shape[2], 1)
    y = y.reshape(y.shape[0], y.shape[1], y.shape[2], 1)

    to_be_predicted.append(x)
    masks.append(y)


scans = np.asarray(to_be_predicted)
masks = np.asarray(masks)




prediction = model.predict(scans)
preds = prediction

np.save(config["workingdir"] + "/" + "masks.npy", masks)
np.save(config["workingdir"] + "/" "preds.npy", preds)
np.save(config["workingdir"] + "/" "scans", scans)

from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure, morphology
import matplotlib.pyplot as plt
import numpy as np






def save_plot_3d(image, filename,  threshold=.3):
    p = image.transpose(2,1,0)
    verts, faces, normals, values = measure.marching_cubes(p, threshold)


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha=0.3)
    face_color = [0.5, 0.5, 1]
    mesh.set_facecolor(face_color)
    ax.add_collection3d(mesh)
    ax.set_xlim(0, p.shape[0])
    ax.set_ylim(0, p.shape[1])
    ax.set_zlim(0, p.shape[2])
    plt.savefig("3dplot_" + str(filename) + ".pdf")






for i in range(preds.shape[0]):

    save_plot_3d(np.squeeze(preds[i]),config["workingdir"] + "/" + str(i) + "pred")
    save_plot_3d(np.squeeze(scans[i]),config["workingdir"] + "/" + str(i) + "scan")
    save_plot_3d(np.squeeze(masks[i]),config["workingdir"] + "/" + str(i) + "mask")