from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from skimage import measure, morphology
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf




preds = np.load("preds.npy")
scans = np.load("scans.npy")
masks = np.load('masks.npy')
print(preds.shape)
scans = np.reshape(np.squeeze(scans), (20224,64,64))
preds = np.reshape(np.squeeze(preds), (20224,64,64))
masks = np.reshape(np.squeeze(masks), (20224,64,64))





def plot_3d(image, mask,  threshold=.3):
    p = image.transpose(2,1,0)
    verts, faces, normals, values = measure.marching_cubes(p, threshold)

    q = image.transpose(2,1,0)
    qverts, qfaces, qnormals, qvalues = measure.marching_cubes(q, threshold)

    qmesh = Poly3DCollection(verts[faces], alpha=0.1)
    qface_color = [0.8, 0.8, 0.8]
    qmesh.set_facecolor(qface_color)


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha=0.1)
    face_color = [0.5, 0.5, 1]
    mesh.set_facecolor(face_color)
    ax.add_collection3d(mesh)
    ax.add_collection3d(qmesh)
    ax.set_xlim(0, p.shape[0])
    ax.set_ylim(0, p.shape[1])
    ax.set_zlim(0, p.shape[2])
    plt.savefig("3dplot_" + str(threshold) + ".pdf")




def multi_slice_viewer_single(volume):
    # Use the "j" and "k" keys to change the scan layer
    def process_key(event):
        fig = event.canvas.figure
        ax = fig.axes[0]
        if event.key == 'j':
            previous_slice(ax, fig)
        elif event.key == 'k':
            next_slice(ax, fig)
        fig.canvas.draw()

    def previous_slice(axis, figure):
        volume = axis.volume
        axis.index = (axis.index - 1) % volume.shape[0]  # wrap around using %
        axis.images[0].set_array(volume[axis.index])
        figure.suptitle('Index:' + str(axis.index))

    def next_slice(axis, figure):
        volume = axis.volume
        axis.index = (axis.index + 1) % volume.shape[0]
        axis.images[0].set_array(volume[axis.index])
        figure.suptitle('Index:' + str(axis.index))

    def remove_keymap_conflicts(new_keys_set):
        for prop in plt.rcParams:
            if prop.startswith('keymap.'):
                keys = plt.rcParams[prop]
                remove_list = set(keys) & new_keys_set
                for key in remove_list:
                    keys.remove(key)

    remove_keymap_conflicts({'j', 'k'})
    figure, axis = plt.subplots()
    axis.volume = volume
    axis.index = volume.shape[0] // 2
    figure.suptitle('Index:' + str(axis.index))
    axis.imshow(volume[axis.index])
    figure.canvas.mpl_connect('key_press_event', process_key)
    plt.show()

def multi_slice_viewer_triple(feats, anns = None, preds = None, num_classes = 1):
    # Plot feats, anns, predictions in multi-slice-view
    # Note this feats OR feats + anns OR feats + anns + preds
    if anns is None:
        fig, ax = plt.subplots()
        ax.volume = feats
        ax.index = feats.shape[-1] // 2
        ax.imshow(feats[ax.index, :, :],  cmap='bone')
        fig.canvas.mpl_connect('key_press_event', process_key)
        plt.show()
    else:
        if preds is None:
            fig, axarr = plt.subplots(1, 2)
            plt.tight_layout()
            axarr[0].volume = feats
            axarr[0].index = 0
            axarr[0].imshow(feats[axarr[0].index, :, :],  cmap='bone')
            axarr[0].set_title('Scans')
            axarr[1].volume = anns
            axarr[1].index = 0
            axarr[1].imshow(anns[axarr[1].index, :, :],  cmap='bone', vmin = 0, vmax = num_classes)
            axarr[1].set_title('Annotations')
            fig.canvas.mpl_connect('key_press_event', process_key)
            plt.show()
        else:
            fig, axarr = plt.subplots(1, 3)
            plt.tight_layout()
            axarr[0].volume = feats
            axarr[0].index = 0
            axarr[0].imshow(feats[axarr[0].index, :, :] ,  cmap='viridis')
            axarr[0].set_title('Scans')
            axarr[1].volume = anns
            axarr[1].index = 0
            axarr[1].imshow(anns[axarr[1].index, :, :])
            axarr[1].set_title('Annotations')
            axarr[2].volume = preds
            axarr[2].index = 0
            axarr[2].imshow(preds[axarr[2].index, :, :])
            axarr[2].set_title('Predictions')
            fig.canvas.mpl_connect('key_press_event', process_key)
            plt.show()

def process_key(event):
    # Process key_press events
    fig = event.canvas.figure
    if event.key == 'j':
        for ax in fig.axes:
            previous_slice(ax)
    elif event.key == 'k':
        for ax in fig.axes:
            next_slice(ax)
    fig.canvas.draw()

def previous_slice(ax):
    """Go to the previous slice."""
    volume = ax.volume
    ax.index = (ax.index - 1) % volume.shape[-1]  # wrap around using %
    ax.images[0].set_array(volume[ax.index, :, :])

def next_slice(ax):
    """Go to the next slice."""
    volume = ax.volume
    ax.index = (ax.index + 1) % volume.shape[-1]
    ax.images[0].set_array(volume[ax.index, :, :])

def remove_keymap_conflicts(new_keys_set):
    for prop in plt.rcParams:
        if prop.startswith('keymap.'):
            keys = plt.rcParams[prop]
            remove_list = set(keys) & new_keys_set
            for key in remove_list:
                keys.remove(key)
remove_keymap_conflicts({'j', 'k'})




def save_plot_3d(image, filename,  threshold=.3):
    p = image.transpose(2,1,0)
    verts, faces, normals, values = measure.marching_cubes(p, threshold)


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    mesh = Poly3DCollection(verts[faces], alpha=0.3)
    face_color = [0.5, 0.5, 1]
    mesh.set_facecolor(face_color)
    ax.add_collection3d(mesh)
    ax.set_xlim(0, p.shape[0])
    ax.set_ylim(0, p.shape[1])
    ax.set_zlim(0, p.shape[2])
    plt.savefig("3dsuperplot_" + str(filename) + ".pdf")

# print(masks.shape)
multi_slice_viewer_triple(scans[10960:], masks[10960:], tf.keras.backend.round(preds[10960:]))
print(sum(masks[10860])>0)


# plt.imshow(masks[10760])
# plt.show()

# print(type(masks[0][4][4]))